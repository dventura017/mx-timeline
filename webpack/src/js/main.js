import axios from 'axios';
import Toastify from 'toastify-js';

// Polyfills
// Elemement.closet Pollyfill
if (!Element.prototype.matches) {
  Element.prototype.matches = Element.prototype.msMatchesSelector ||
                              Element.prototype.webkitMatchesSelector;
}

if (!Element.prototype.closest) {
  Element.prototype.closest = function(s) {
    var el = this;

    do {
      if (el.matches(s)) return el;
      el = el.parentElement || el.parentNode;
    } while (el !== null && el.nodeType === 1);
    return null;
  };
}

// General Variables
const body = document.querySelectorAll('body')[0];

// Milestone open
const milestones = document.querySelectorAll('.milestone')
const milestoneDots = document.querySelectorAll('.milestone__dot')

const onClickMilestoneDot = e => {
  const milestone = e.currentTarget.parentNode

  e.preventDefault()

  if (milestone.classList.contains('milestone--active')) {
    milestone.classList.remove('milestone--active')
  } else {
    Array.prototype.forEach.call(milestones, milestone => milestone.classList.remove('milestone--active'))
    milestone.classList.add('milestone--active')
  }
}

// Milestone add open
const milestoneOpenButton = document.querySelector('.add-milestone__open')
const milestoneCloseButton = document.querySelector('.add-milestone__close')
const milestoneAddFormWrapper = document.querySelector('.add-milestone__form')
const milestoneAddForm = document.querySelector('.add-milestone__form form')

const onClickMilestoneOpen = () => {
  milestoneAddFormWrapper.classList.add('add-milestone__form--active')
  milestoneOpenButton.classList.add('add-milestone__button--disable')
}

const onClickMilestoneClose = () => {
  milestoneAddFormWrapper.classList.remove('add-milestone__form--active')
  milestoneOpenButton.classList.remove('add-milestone__button--disable')
}

const showToast = (message, isError) => {
    Toastify({
        text: message,
        duration: 3000,
        close: true,
        gravity: 'top',
        position: 'right',
        backgroundColor: isError ? 'linear-gradient(to right, #f1a5ad, #f5c6cb)' : 'linear-gradient(to right, #90d8a0, #b8ecc5)'
    }).showToast()
}

const onFormMilestoneSubmit = e => {
  e.preventDefault();

  axios
    .post(
      '/timeline/add',
      new FormData(milestoneAddForm)
      )
      .then(
          (response) => {
            if (typeof response.data.error === 'undefined') {
                showToast('¡Milestone agregado correctamente!', false)
                onClickMilestoneClose()
            } else {
                showToast(response.data.error, true);
            }
          }
      )
      .catch(
          (err) => {
              showToast('Ocurrió un error al enviar el formulario', true)
          }
      );
}

const onClickDeleteMilestone = e => {
  e.preventDefault();

  axios
      .post('/timeline/'+e.target.parentNode.getAttribute('data-id')+'/delete')
      .then(
          () => {
            const milestone = e.target.closest('.milestone')
            const parent = milestone.parentNode
            parent.removeChild(milestone)
          }
      )
      .catch(
          () => {
            showToast('Ocurrió un error al eliminar.', true)
          }
      );
}

// Change logo
const year2007 = document.querySelector('[data-year="2007"]')
const year2016 = document.querySelector('[data-year="2016"]')
const logo = document.querySelector('.header__brand__image')

const changeLogo = () => {
  if( year2007.offsetTop <= 10 ) {
    logo.setAttribute('src', 'assets/images/logo_mx_1.jpg');
  }
  if( year2007.offsetTop >= 10 ) {
    logo.setAttribute('src', 'assets/images/logo_mx_2.jpg');
  }
  if( year2016.offsetTop >= 10 ) {
    logo.setAttribute('src', 'assets/images/logo_mx_3.jpg');
  }
}

// Intro
const intro = document.querySelector('.intro')
const introButton = document.querySelector('.intro__button')
const hideIntro = () => {
  intro.classList.add('d-none')
}

// Init
const init = () => {

  // Milestone open init
  Array.prototype.forEach.call(milestoneDots, milestoneDot => {
    milestoneDot.addEventListener('click', onClickMilestoneDot, false);
  });

  // Milestone add init
  if ( milestoneOpenButton ) {
    milestoneOpenButton.addEventListener('click', onClickMilestoneOpen, false);
  }

  if ( milestoneCloseButton ) {
    milestoneCloseButton.addEventListener('click', onClickMilestoneClose, false);
  }

  if ( milestoneAddForm ) {
    milestoneAddForm.addEventListener('submit', onFormMilestoneSubmit, false);
  }

  window.addEventListener('scroll', changeLogo);

  if ( introButton ) {
    introButton.addEventListener('click', hideIntro, false);
  }

  document.querySelectorAll('.milestone__erase').forEach((el) => el.addEventListener('click', onClickDeleteMilestone, false))
};

document.addEventListener('DOMContentLoaded', init);
