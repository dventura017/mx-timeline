jQuery.get(
  'http://multiplica.net/timeline/list',
  function(response) {
      // el campo 'editable' indica si el usuario actual tiene permiso para editar el contenido
      /*
      [
         {
            "id":1,
            "user":{
               "id":207,
               "name":"Rodrigo Souto",
               "country":"Buenos Aires",
               "company":"Multiplica"
            },
            "year":"2018",
            "month":"12",
            "title":"test",
            "editable":true
         },
         {
            "id":2,
            "user":{
               "id":207,
               "name":"Rodrigo Souto",
               "country":"Buenos Aires",
               "company":"Multiplica"
            },
            "year":"2018",
            "month":"12",
            "title":"test",
            "editable":true
         },
         {
            "id":7,
            "user":{
               "id":207,
               "name":"Rodrigo Souto",
               "country":"Buenos Aires",
               "company":"Multiplica"
            },
            "year":"2019",
            "month":null,
            "title":"test",
            "editable":true
         }
      ]
      */
      console.log(JSON.stringify(response));
  }
);

jQuery.post(
  'http://multiplica.net/timeline',
  {title: 'test', 'year': 2019, 'month': 12}, // month = 0 si no tiene mes
  function(response) {
      // devuelve el objeto creado o {error: 'xxx'} en caso de error
      alert(JSON.stringify(response));
  }
);

jQuery.post(
  'http://multiplica.net/timeline/7/edit',
  {title: 'test', 'year': 2020, 'month': 6}, // month = 0 si no tiene mes
  function(response) {
      // devuelve el objeto editado o {error: 'xxx'} en caso de error
      alert(JSON.stringify(response));
  }
);

jQuery.post(
  'http://multiplica.net/timeline/12/delete',
  {},
  function(response) {
      // devuelve {error: 'xxx'} en caso de error
      alert(JSON.stringify(response));
  }
);